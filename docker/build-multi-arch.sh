#!/bin/bash

IMAGE=nomadiclabs/snapcraft_cross

BUILD_ARCHS="x86_64 armhf arm64"
for docker_arch in ${BUILD_ARCHS}; do
  case ${docker_arch} in
    x86_64 ) qemu_arch="x86_64" ;;
    armhf  ) qemu_arch="arm" ;;
    arm64  ) qemu_arch="aarch64" ;;
  esac
  cp Dockerfile.cross Dockerfile.${docker_arch}
  sed -i "s|__QEMU_ARCH__|${qemu_arch}|g" Dockerfile.${docker_arch}
  sed -i "s|__BASEIMAGE_ARCH__|${docker_arch}|g" Dockerfile.${docker_arch}
  if [ ${docker_arch} == 'x86_64' ]; then
    sed -i "/__CROSS_/d" Dockerfile.${docker_arch}
  else
    [ ! -f qemu-${qemu_arch}-static ] && cp /usr/bin/qemu-${qemu_arch}-static .
    sed -i "s/__CROSS_//g" Dockerfile.${docker_arch}
  fi
done

for arch in ${BUILD_ARCHS}; do
  docker build --no-cache -f Dockerfile.${arch} -t ${IMAGE}:${arch}-latest .
  docker push ${IMAGE}:${arch}-latest
done

#docker manifest create -a ${IMAGE}:latest \
	#${IMAGE}:x86_64-latest \
	#${IMAGE}:armhf-latest \
	#${IMAGE}:arm64-latest \

#docker manifest annotate ${IMAGE}:latest \
	#${IMAGE}:x86_64-latest --os linux --arch amd64

#docker manifest annotate ${IMAGE}:latest \
	#${IMAGE}:armhf-latest --os linux --arch arm

#docker manifest annotate ${IMAGE}:latest \
	#${IMAGE}:arm64-latest --os linux --arch arm64 --variant armv8

#docker manifest push ${IMAGE}:latest
