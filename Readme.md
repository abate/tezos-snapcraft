# Tezos Snap distribution

## Download the snap           

    wget https://gitlab.com/abate/tezos-snapcraft/raw/master/snaps/tezos_5.1.0_multi.snap
    
It is recommended to check the sha256 of the file

## Install the snap
    
    sudo snap install --dangerous tezos_1.0_multi.snap

`--dangerous` is to acknoledge that the snap is not signed.

This will be fixed in the future

## Using the snap is tezos as usual

    $ export PATH=/snap/bin:$PATH
    $ tezos.client man

## Build Tezos snaps

The snap packages are built from the master branch of the tezos repository.

    $ create-snaps.sh

The result will be copied in the `snaps` directory.

The `docker` directory contains the Dockerfiles used for the compilation.
To update the docker images you can use the provided `Makefile`
