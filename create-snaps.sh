#!/bin/bash

set -e

mkdir -p snaps

copy() {
docker run -i \
  -v tezos-snapcraft_build:/build \
  -v "${PWD}/snaps:/snaps" -u $(id -u) \
    busybox sh << COMMANDS
cp /build/*.snap /snaps/
COMMANDS
}

snap() {
  arch=$1
	docker system prune --force
	docker volume rm tezos-snapcraft_build || true
	ARCH="$arch" SNAP=snapcraft.yaml docker-compose run build
	copy
}


snap $1

exit 0

snap x86_64
snap arm64
snap armhf
